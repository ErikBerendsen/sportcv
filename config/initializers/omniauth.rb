OmniAuth.config.logger = Rails.logger

Rails.application.config.middleware.use OmniAuth::Builder do

  provider :facebook, ENV["FACEBOOK_ID"], ENV["FACEBOOK_SECRET_KEY"],
  :client_options => {:ssl => {:verify => false}},
  :scope => 'email,user_birthday,read_stream,image',
  :display => 'popup', 
  :provider_ignores_state => true,
  image_size: {width: '200', height: '200'}

end

#TAKE DOWN :client_options... for production!
