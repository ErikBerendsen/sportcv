class AddMessageCheckBooleanToManager < ActiveRecord::Migration
  def change
  	add_column :managers, :has_message, :boolean, :default => false
  end
end
