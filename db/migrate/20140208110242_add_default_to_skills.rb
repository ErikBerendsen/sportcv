class AddDefaultToSkills < ActiveRecord::Migration
  def change
  	change_column :skills, :speed, :int, :default => 1
  	change_column :skills, :control, :int, :default => 1
  	change_column :skills, :shooting, :int, :default => 1
  	change_column :skills, :passing, :int, :default => 1
	change_column :skills, :endurance, :int, :default => 1
  end
end
