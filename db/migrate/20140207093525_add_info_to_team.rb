class AddInfoToTeam < ActiveRecord::Migration
  def change
    add_column :teams, :name, :string
    add_column :teams, :league, :string
    add_column :teams, :country, :string
    add_column :teams, :manager, :string
    add_column :teams, :established, :date
    add_column :teams, :email, :string
    add_column :teams, :webpage, :string
  end
end
