class CreateSkills < ActiveRecord::Migration
  def change
    create_table :skills do |t|
      t.integer :speed
      t.integer :control
      t.integer :shooting
      t.integer :passing
      t.integer :endurance
    end
  end
end
