class AddDescImgVidUrlFieldToTeam < ActiveRecord::Migration
  def change
    add_column :teams, :desc, :text
    add_column :teams, :image_url, :string
    add_column :teams, :video_url, :string
  end
end
