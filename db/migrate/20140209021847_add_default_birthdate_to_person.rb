class AddDefaultBirthdateToPerson < ActiveRecord::Migration
  def change
  	change_column :people, :birthday, :date, :default => Date.current
  end
end
