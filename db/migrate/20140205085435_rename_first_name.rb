class RenameFirstName < ActiveRecord::Migration
  def change
    rename_column :people, :firstname, :first_name
  end
end
