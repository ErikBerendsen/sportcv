class AddPlayerIdToExtra < ActiveRecord::Migration
  def change
    add_column :extras, :player_id, :int
  end
end
