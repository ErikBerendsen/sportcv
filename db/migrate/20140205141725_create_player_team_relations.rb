class CreatePlayerTeamRelations < ActiveRecord::Migration
  def change
    create_table :player_team_relations do |t|
      t.integer :player_id
      t.integer :team_id
      t.boolean :player_accepted?
      t.boolean :team_accepted?
      t.string :offer_made_at
      t.string :datetime

      t.timestamps
    end
  end
end
