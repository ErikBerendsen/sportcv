class AddEmailToPerson < ActiveRecord::Migration
  def change
    add_column :people, :email, :string
    add_column :people, :height, :float
    add_column :people, :weight, :float
  end
end
