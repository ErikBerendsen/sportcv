class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.string :firstname
      t.date :birthday
      t.string :position

      t.timestamps
    end
  end
end
