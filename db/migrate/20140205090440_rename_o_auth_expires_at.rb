class RenameOAuthExpiresAt < ActiveRecord::Migration
  def change
    rename_column :people, :oauth_expires_at_at, :oauth_expires_at
  end
end
