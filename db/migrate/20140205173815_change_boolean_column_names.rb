class ChangeBooleanColumnNames < ActiveRecord::Migration
  def change
  	rename_column :player_team_relations, :team_accepted?, :team_accepted
  	rename_column :player_team_relations, :player_accepted?, :player_accepted
  end
end
