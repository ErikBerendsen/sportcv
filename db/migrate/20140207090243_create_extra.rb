class CreateExtra < ActiveRecord::Migration
  def change
    create_table :extras do |t|
      t.string :prev_teams
      t.string :achievements
      t.string :recommendations
      t.string :promo_vid
    end
  end
end
