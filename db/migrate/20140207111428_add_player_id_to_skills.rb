class AddPlayerIdToSkills < ActiveRecord::Migration
  def change
    add_column :skills, :player_id, :integer
  end
end
