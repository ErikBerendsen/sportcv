class CreatePlayers < ActiveRecord::Migration
  def change
    create_table :players do |t|
      t.integer :person_id

      t.timestamps
    end
  end
end
