# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140209025901) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "create_model_skills", force: true do |t|
    t.integer  "speed"
    t.integer  "control"
    t.integer  "shooting"
    t.integer  "passing"
    t.integer  "endurance"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "extras", force: true do |t|
    t.string  "prev_teams"
    t.string  "achievements"
    t.string  "recommendations"
    t.string  "promo_vid"
    t.integer "player_id"
  end

  create_table "managers", force: true do |t|
    t.integer  "person_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "team_id"
    t.boolean  "has_message", default: false
  end

  create_table "people", force: true do |t|
    t.string   "first_name"
    t.date     "birthday",         default: '2014-02-09'
    t.string   "position"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "provider"
    t.string   "uid"
    t.string   "oauth_token"
    t.datetime "oauth_expires_at"
    t.string   "last_name"
    t.string   "email"
    t.float    "height"
    t.float    "weight"
    t.integer  "phone"
    t.string   "image_url"
  end

  create_table "player_team_relations", force: true do |t|
    t.integer  "player_id"
    t.integer  "team_id"
    t.boolean  "player_accepted"
    t.boolean  "team_accepted"
    t.string   "offer_made_at"
    t.string   "datetime"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "players", force: true do |t|
    t.integer  "person_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "desc"
  end

  create_table "roles", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "skill_player_relations", force: true do |t|
    t.integer  "speed"
    t.integer  "control"
    t.integer  "shooting"
    t.integer  "passing"
    t.integer  "endurance"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "skills", force: true do |t|
    t.integer "speed",     default: 1
    t.integer "control",   default: 1
    t.integer "shooting",  default: 1
    t.integer "passing",   default: 1
    t.integer "endurance", default: 1
    t.integer "player_id"
  end

  create_table "sports", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "teams", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "league"
    t.string   "country"
    t.date     "established"
    t.string   "email"
    t.string   "webpage"
    t.text     "desc"
    t.string   "image_url"
    t.string   "video_url"
  end

end
