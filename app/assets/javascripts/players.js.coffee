# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on('click', '.progress-edit', (e) ->
	rating = e.pageX - $(this).offset().left
	$(this).children('.progress-bar').css('width',rating)
	# Gives player rating
	$(this).children('.text_field').val(Math.ceil(rating/$(this).width()*100)))

-$(window).load ->
-	$('#skills_eval').css("height",$('#info').css("height"))