json.array!(@people) do |person|
  json.extract! person, :id, :firstname, :birthday, :email, :phone, :height, :weight, :position
  json.url person_url(person, format: :json)
end
