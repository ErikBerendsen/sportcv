class PlayerTeamRelationsController < ApplicationController

  before_action :set_player_team_relation, only: [:destroy]


  def create
  	@relation = PlayerTeamRelation.new(player_team_relation_params)
  	@relation.team_accepted = false
  	@relation.player_accepted = true
    manager = @relation.team.manager
    manager.has_message = true
    manager.save

  	respond_to do |format|
      if @relation.save
        format.js {render 'create.js.coffee.erb'}
      end
    end

  end

  def destroy
    @to_remove = @player_team_relation.id
    @player_team_relation.destroy

    respond_to do |format|
    	format.js {render 'destroy.js.coffee.erb'}
    end
  end





  private
    # Use callbacks to share common setup or constraints between actions.
    def set_player_team_relation
      @player_team_relation = PlayerTeamRelation.find(params[:id])
    end
    def player_team_relation_params
      params.require(:player_team_relation).permit(:player_id,:team_id)
    end

end