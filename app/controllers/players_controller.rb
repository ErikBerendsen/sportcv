class PlayersController < ApplicationController
  before_action :set_player, only: [ :edit, :update, :destroy, :show_connections]

  # GET /players
  # GET /players.json
  def index
    @players = Player.all
  end

  # GET /players/1
  # GET /players/1.json
  def show
    @player = Player.find_by_id(params[:id])

    respond_to do |format|
      if not @player
        format.html { redirect_to players_url, alert: 'Player was not found.' }
      else
        format.html
      end
    end
  end

  # GET /players/new
  def new
    @player = Player.new
  end

  # GET /players/1/edit
  def edit
    @person = @player.person
    @skill = @player.skill
  end

  # POST /players
  # POST /players.json
  def create
    @player = Player.new_with_skill(player_params)



    respond_to do |format|
      if @player.save
        format.html { redirect_to @player, notice: 'Player was successfully created.' }
        format.json { render action: 'show', status: :created, location: @player }
      else
        format.html { render action: 'new' }
        format.json { render json: @player.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /players/1
  # PATCH/PUT /players/1.json
  def update
    @person = @player.person

    respond_to do |format|
      if @player.update(player_params) && @player.person.update(person_params)
        format.html { redirect_to @player, notice: 'Player was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @player.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /players/1
  # DELETE /players/1.json
  def destroy
    @player.destroy
    respond_to do |format|
      format.html { redirect_to players_url }
      format.json { head :no_content }
    end
  end

  def show_connections
    @connections = @player.new_connections
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_player
      @player = Player.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def player_params
      params.require(:player).permit(:person_id, :desc, skill_attributes: [ :id, :speed, :control, :shooting, :passing, :endurance]) 
    end

    def person_params
      params.require(:person).permit(:first_name, :last_name, :birthday, :email, :height, :weight, :phone, :position)
    end
end
