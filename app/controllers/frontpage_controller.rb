class FrontpageController < ApplicationController
  after_action :allow_iframe, only: :index

  def index
  	if session[:user_id].present? 
  	  user = Person.find(session[:user_id])
  	end
  	respond_to do |format|
  	  if user
  	    if user.is_a_new_user?
          format.html { render controller: 'frontpage', action: 'first_time_user'}
        else
      	  if user.is_a_player?
      		format.html { redirect_to player_path(user.players.first) }
      	  else
      	    format.html { redirect_to manager_path(user.managers.first) }
          end
        end
      else
        format.html {render layout: 'frontpage_layout' }
      end
    end
  end

  def first_time_user
  	render layout: 'frontpage_layout'
  end

  private
  
  def allow_iframe
    response.headers.except! 'X-Frame-Options'
  end
end
