
class SessionsController < ApplicationController
  def create
    user = Person.from_omniauth(env["omniauth.auth"])
    session[:user_id] = user.id
    respond_to do |format|
      if user.is_a_new_user?
       format.html { redirect_to controller: 'frontpage', action: 'first_time_user'}
      else
      	if user.is_a_player?
      		format.html { redirect_to player_path(user.players.first) }
      	else
      		format.html { redirect_to team_path(user.managers.first.team) }
        end
      end
    end
  end

  def destroy
    session[:user_id] = nil
    sleep 3
    redirect_to root_url
  end
end


