class Team < ActiveRecord::Base
	belongs_to :sport
	has_many :extras
	has_many :player_team_relations
	has_many :players, through:  :player_team_relations
	has_one :manager

	def iniate_contact(player) 
		player_team_relation = PlayerTeamRelation.new
		player_team_relation.team = self
		player_team_relation.player = player
		player_team_relation.team_accepted = true
		player_team_relation.player_accepted = false
		player_team_relation.save
	end
end
