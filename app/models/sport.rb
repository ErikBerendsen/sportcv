class Sport < ActiveRecord::Base
	has_many :roles
	has_many :players, through: :roles
	has_many :teams
end
