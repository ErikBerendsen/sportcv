class Manager < ActiveRecord::Base
	belongs_to :person	
	belongs_to :team, autosave: true


	def new_connections
		self.team.player_team_relations.find_all { |rel| !rel.player_accepted}
	end
end
