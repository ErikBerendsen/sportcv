class Person < ActiveRecord::Base

  has_many :managers, :dependent => :destroy
  has_many :players, :dependent => :destroy

  def is_a_new_user?
     self.players.empty? && self.managers.empty?
  end

   def is_a_player?
     not self.players.empty?
  end

   def is_a_manager?
     not self.managers.empty?
   end

  def self.from_omniauth(auth) 
    where(auth.slice(:provider, :uid)).first_or_initialize.tap do |person|
      person.provider = auth.provider
      person.uid = auth.uid
      person.first_name = auth.info.first_name
      person.last_name = auth.info.last_name
      person.image_url = auth.info.image
      person.birthday = auth.extra.raw_info.birthday if auth.extra.raw_info.birthday.present?
      person.email = auth.extra.raw_info.email
      person.oauth_token = auth.credentials.token
      person.oauth_expires_at = Time.at(auth.credentials.expires_at)
      person.save!
    end
  end


 


end
