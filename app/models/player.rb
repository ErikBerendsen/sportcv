class Player < ActiveRecord::Base
	validates_length_of :desc, :minimum => 2, :maximum => 500, :allow_blank => true
	validates :person_id, presence: true, numericality: { only_integer: true }, :allow_blank => false


	has_one :skill
	accepts_nested_attributes_for :skill

	has_many :sports
	has_one :extra
	has_one :role, through: :sports
	has_many :player_team_relations
	has_many :teams, through: :player_team_relations

	belongs_to :person
	


	def new_connections?
		self.player_team_relations.exists?(player_accepted: false)
	end

	def accept_connection(player_team_relations)
      player_team_relations.player_accepted = true
      player_team_relations.save
	end

	def self.new_with_skill(params)
		player = Player.new(params)
		player.skill = Skill.new
		player
	end

	def new_connections
		self.player_team_relations.find_all { |rel| !rel.player_accepted}
	end

end
