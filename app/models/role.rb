class Role < ActiveRecord::Base
	has_one :player
	belongs_to :sport
end
